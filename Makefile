BEAM_MODULE:=mdm_challenge_python.mdm_challenge_main
BEAM_MODULE_PUBSUB:=mdm_challenge_python.mdm_challenge_main_pubsub
BUCKET_NAME:=mdm-challenge
CLOUD_FUNCTION_ENTRYPOINT:=trigger_dataflow_job
CLOUD_FUNCTION_NAME:=trigger_dataflow_job
CLOUD_FUNCTION_RUNTIME:=python37
CLOUD_FUNCTION_SOURCE:=gcloud_function
CLOUD_FUNCTION_TRIGGER_BUCKET:=${BUCKET_NAME}-inputs
DATAFLOW_RUNNER:=DataflowRunner
GCLOUD_PROJECT_ID:=jlg-sandbox
GCLOUD_REGION:=europe-west1
PROJECT_PATH:=${PWD}/mdm_challenge_python
TEMPLATE_LOCATION:=gs://${BUCKET_NAME}/templates/mdm_challenge_dataflow_template
TMP_LOCATION:=gs://${BUCKET_NAME}/tmp/

run-precommit:
	pre-commit run --all-files

run-tests:
	pip install ${PROJECT_PATH}
	python ${PROJECT_PATH}/setup.py test

run-local:
	python -m ${BEAM_MODULE} --input_file ${input_file} \
    						 --output_path ${output_path}

run-dataflow:
	python -m ${BEAM_MODULE} --input_file ${input_file} \
							 --output_path ${output_path} \
							 --runner ${DATAFLOW_RUNNER} \
							 --project ${GCLOUD_PROJECT_ID} \
							 --region ${GCLOUD_REGION} \
							 --temp_location ${TMP_LOCATION} \
							 --setup_file ${PROJECT_PATH}/setup.py

deploy-template:
	pip install ${PROJECT_PATH}
	python -m ${BEAM_MODULE} \
			--runner ${DATAFLOW_RUNNER} \
			--region ${GCLOUD_REGION} \
			--project ${GCLOUD_PROJECT_ID} \
			--temp_location ${TMP_LOCATION} \
			--template_location ${TEMPLATE_LOCATION} \
			--setup_file ${PROJECT_PATH}/setup.py

deploy-cloud-function:
	gcloud functions deploy ${CLOUD_FUNCTION_NAME} \
						 --source ${CLOUD_FUNCTION_SOURCE} \
						 --entry-point ${CLOUD_FUNCTION_ENTRYPOINT} \
						 --set-env-vars PROJECT_ID=${GCLOUD_PROJECT_ID},TEMPLATE=${TEMPLATE_LOCATION} \
						 --runtime ${CLOUD_FUNCTION_RUNTIME} \
						 --region ${GCLOUD_REGION} \
						 --trigger-resource ${CLOUD_FUNCTION_TRIGGER_BUCKET} \
						 --trigger-event google.storage.object.finalize

run-template:
	gcloud dataflow jobs run manually-triggered-dataflow-$(shell date +%s) \
		--region ${GCLOUD_REGION} \
		--gcs-location ${TEMPLATE_LOCATION}
		--parameters input_file=${input_file},output_path=${output_path}