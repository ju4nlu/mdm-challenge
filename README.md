# mdm-challenge
[![pipeline status](https://gitlab.com/ju4nlu/mdm-challenge/badges/master/pipeline.svg)](https://gitlab.com/ju4nlu/mdm-challenge/-/commits/master) [![Python 3.7.9](https://img.shields.io/badge/python-3.7.9-blue.svg)](https://www.python.org/downloads/release/python-379/) [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

[[_TOC_]]

## Packages and dependencies.

- Python version: 3.7.0
- Apache Beam SDK version: 2.27.0
- All of the Apache Beam SDK dependencies are pinned in the `./requirements.txt` file.
- Code format uses [black](https://github.com/psf/black).
- Pre-commit configuration with `check-yaml`, `end-of-file-fixer`, `trailing-whitespace` and _black_ formatters. [(More info here)](https://pre-commit.com/)

Note that the Apache Beam SDK was installed together with the extra packages `gcp` and `test`. More info about this on the [official quickstart site](https://beam.apache.org/get-started/quickstart-py/).

## Architecture

The actual implementation in production is though to process the files in a batch mode. This is the diagram of the architecture:
![Batch Architecture](./images/batch_architecture.png)

The behaviour would be:
1. A file is uploaded to a specific Bucket.
1. A Cloud Function is triggered whenever there is a new file in that Bucket.
1. The Cloud Function runs the Dataflow job with the new file name as input argument.
1. The file is processed and the results stored in the predefined output path.

In the case where we are interested in processing the files in a streaming mode, the architecture will change to:
![Streaming Architecture](./images/streaming_architecture.png)

In this case, the behaviour would change to:
1. A file is uploaded to a specific Bucket.
1. **A PubSub message is automatically send to a topic. This message will contain information about the new uploaded file.**
1. **A Dataflow job which is constantly running will process as many files/lines as specified by the window size.**
1. The results of each window will be stored in the predefined output path.

## Deployment and production runs

There is a GitLab pipeline that will handle the testing and deployment of all the components. For each commit, the behaviour will defer depending on the branch we are pushing to:

- Anyone but `master`: just the tests will be run.
- `master`: the tests run, if they pass, then the pipeline template is deployed to Dataflow and the Cloud Function is deployed as well.

If we want to run a pipeline in **production**, we could just upload a new file to the bucket `mdm-challenge-inputs`. This bucket name is specified in the Makefile by the variable `CLOUD_FUNCTION_TRIGGER_BUCKET` and could be changed when deploying if needed.

We could also run a pipeline from our local machine by execution the Makefile recipe `run-dataflow`. More details about this process below.

Running this pipeline will generate 3 files in the `output_path` directory:
- `1_indicator.txt`: Result of the first question where each line is a tuple of (Country, Average daily use of internet).
- `2_indicator.txt`: Result of the second question which is a number ranging from 0 to 1 indicating the percentage of people under 35 that have ever had a paid job.
- `3_indicator.txt`: Result of the third question, it's a dictionary where each key corresponds to a net income level and its value is the most represented education level for that net income level.

## Project structure

The directory structure of the project is as follows:
```
.
├── gcloud_function/
├── previous_data_exploration/
├── mdm_challenge_python/
│   ├── mdm_challenge/
│   ├── mdm_challenge_main.py
│   ├── setup.py
│   └── tests/
├── .gcloudignore
├── .gitignore
├── .gitlab-ci.yml
├── .pre-commit-config.yaml
├── Makefile
└── README.md
```

- `gcloud_function` is a directory that contains the code of the Google Cloud Function used to trigger the Dataflow jobs.
- `previous_data_exploration` contains the original challenge, the input data and a Jupyter Notebook that I've used to make a first approach to the dataset and the problem.
- `mdm_challenge_python` contains the Apache Beam Python project files
  - `mdm_challenge` is where all code which is not a pipeline is placed. This includes PTransforms and Options files at the moment.
  - `mdm_challenge_main.py` is the file where the Pipelines are declared.
  - `setup.py` is used to package the project, and it's needed when deploying the pipelines to Dataflow as well.
  - `tests` is the directory where all test files are placed.

The Python project has been structured according to the [Apache Beam Python SDK docs regarding dependencies](https://beam.apache.org/documentation/sdks/python-pipeline-dependencies/).


## Manually running the pipelines

In order to ease the execution of the pipelines, there is a `Makefile` in the root directory where the execution modes have been encapsulated. That Makefile contains several variables with predefined values that can be easily overwritten before running:
- `BUCKET_NAME`: Default bucket name used to run the pipeline.
- `DATAFLOW_RUNNER`: Name of the Dataflow runner that needs to be specified when running a Pipeline in Dataflow.
- `GCLOUD_PROJECT_ID`: Google Cloud project ID. It's important to note that sometimes the project name is not equal to its ID.
- `GCLOUD_REGION`: Region where the Pipeline will be executed.

Then, the file contains the following recipes:
- **run-tests**: It will install the package and run the tests with the `unittest` library.
- **run-local**: Runs the Pipelines in the local environment with the DirectRunner.
- **run-dataflow**: Runs the Pipelines in Dataflow with its correspondant DataflowRunner.

More details about running these recipes and its insights in the following sections.

### Tests (run-tests)

Before running the tests, the package is installed with all of its dependencies. Once this is done, the tests are ran using the suite indicated in the `setup.py` file. In this case, all tests are ran using [unittest](https://docs.python.org/3/library/unittest.html).

Running the tests requires no input parameters, and can be done like so:
```bash
make run-tests
```

At the moment, the results are not stored anywhere, just printed in the console.

### Locally (run-local)

It's possible to run the pipelines using the Apache Beam `DirectRunner`. It's done running the module where the pipeline that will be executed is located. This behaviour has been encapsulated in the recipe `run-local`, which accepts two input parameters:
- `--input_file`: Path where the input file is located, it could be either in local storage or in a GCloud bucket.
- `--output_path`: Path where the results will be stored, it could be either in local storage or in a GCloud bucket.

An execution example would be:
```bash
make run-local \
        input_file=gs://mdm-challenge/input_data/ESS9e03.0_F1.csv \
        output_path=./results
```

### Dataflow (run-dataflow)

Before running any Pipeline, there are some [requisites](https://beam.apache.org/documentation/runners/dataflow/) that need to be fulfilled. Both in the machine that will send the request to run the job:
- First of all, it's needed to have the [Google Cloud SDK installed](https://cloud.google.com/sdk/docs/install?hl=en).
- It's also needed to have a working [Service Account ](https://cloud.google.com/iam/docs/service-accounts)
- The environment variable `GOOGLE_APPLICATION_CREDENTIALS` pointing towards the file where the Service Account key is stored.

And in the Google Cloud project it's needed to:
- Assign the correct permissions to the Service Account (it'd be recommended to create a Service Account with minimal permissions to run pipelines). At least, the `Dataflow Admin` and `Cloud Dataflow Service Agent` roles will be needed.
- Enable the Dataflow API in the project settings.

Once the setup is complete, executing a Pipeline in Dataflow is fairly similar to executing it in local. If we are OK with the default configuration values, we will only need to specify the `--input_file` and `--output_path` parameters as we did in the Direct Runner execution. Note that in this case we'll only be able to use files that are located in a bucket.

Here is an example:
```bash
make run-dataflow \
        input_file=gs://mdm-challenge/input_data/ESS9e03.0_F1.csv \
        output_path=gs://mdm-challenge/output_data
```

Let's say that we want to set the region where the Pipeline is running to `us-east1`. Then, we can overwrite the default values like this:
```bash
make run-dataflow \
        GCLOUD_REGION=us-east1 \
        input_file=gs://mdm-challenge/input_data/ESS9e03.0_F1.csv \
        output_path=gs://mdm-challenge/output_data
```

### Pre-commit checks (run-precommit)

This recipe runs all the pre-commit checks and style formatters added in the pre-commit configurations. These are:
- Check Yaml
- Fix End of Files
- Trim Trailing Whitespace
- `black`

Running this command while execute all the checks and the `black` formatter for all the files. It's pretty useful if we've changed many lines of code, as some times the automatic pre-commit unstages some changes and it can be annoying.

## There are plenty of things left to do

Even though I'm quite happy with the processes and how they interact which each other, I think there is still a lot of space for improvement. Some of the aspects that could be improved are:
- **Logging and metrics**: It's not fun to be blind in a production environment, we need to spot and be alerted of possible failures. As it is now, no alerts or metrics are being stored regarding the Dataflow jobs or the Cloud Functions executions. At least, it would be nice to add some alerts in case something fails.
- **Better testing**: It would be nice to add some coverage metrics, or to have [integration tests](https://cloud.google.com/solutions/building-production-ready-data-pipelines-using-dataflow-developing-and-testing#integration_tests) to completely test our pipelines. All the customs PTransforms have been tested, even though some more tests cases could be added.
- **Error handling in the pipeline (dead letter pattern)**: Google recommends following the _dead letter pattern_, which involves branching errors away from the main pipeline path into either a dead letter queue for manual intervention or a programmatic correction path [(Source)](https://cloud.google.com/blog/products/data-analytics/tips-and-tricks-to-get-your-cloud-dataflow-pipelines-into-production). I went trough the happy path with my pipeline, so unexpected errors could go undetected.
- **Pipeline profilling**: If we want to run this pipeline in production, at some time it would be needed to profile it so we can avoid bottle necks in any part of it. I found a [pretty interesting article](https://medium.com/google-cloud/profiling-apache-beam-python-pipelines-d3cac8644fa4), but I didn't find the time to apply it.
- **Deploy just what's changed**: Right now, with every push to `master`, both the Dataflow template and the Cloud Function will be deployed to production. Given that this process takes around 10-15 minutes, we could improve this times by only deploying those components which have been modified in said commit or feature.
