import os
from googleapiclient.discovery import build


def trigger_dataflow_job(event, context):
    """Background Cloud Function to be triggered by Cloud Storage.
       This generic function logs relevant data when a file is changed.

    Args:
        event (dict):  The dictionary with data specific to this type of event.
                       The `data` field contains a description of the event in
                       the Cloud Storage `object` format described here:
                       https://cloud.google.com/storage/docs/json_api/v1/objects#resource
        context (google.cloud.functions.Context): Metadata of triggering event.
    Returns:
        None; the output is written to Stackdriver Logging
    """

    print("Event ID: {}".format(context.event_id))
    print("Event type: {}".format(context.event_type))
    print("Bucket: {}".format(event["bucket"]))
    print("File: {}".format(event["name"]))
    print("Metageneration: {}".format(event["metageneration"]))
    print("Created: {}".format(event["timeCreated"]))
    print("Updated: {}".format(event["updated"]))

    PROJECT = os.environ["PROJECT_ID"]
    TEMPLATE = os.environ["TEMPLATE"]

    job = PROJECT + "_" + str(event["timeCreated"])
    input_file = "gs://" + str(event["bucket"]) + "/" + str(event["name"])
    parameters = {
        "input_file": input_file,
        "output_path": "gs://mdm-challenge/output_data",
    }
    environment = {"tempLocation": "gs://mdm-challenge/tmp"}
    service = build("dataflow", "v1b3", cache_discovery=False)

    request = (
        service.projects()
        .locations()
        .templates()
        .launch(
            projectId=PROJECT,
            gcsPath=TEMPLATE,
            location="europe-west1",
            body={"jobName": job, "parameters": parameters, "environment": environment},
        )
    )
    response = request.execute()
    print(str(response))
