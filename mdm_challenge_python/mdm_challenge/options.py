from apache_beam.options.pipeline_options import PipelineOptions


class PipelineCustomOptions(PipelineOptions):
    """This class implementes specific input options that
    are needed in order to run some pipeline. It extends the
    default class used to pass options to Apache Beam
    pipelines, PipelineOptions.

    The arguments introduced in this class are:
        - input: Used to specify an input file for the pipeline
        - output: Path were results of the pipeline will be stored

    To use this class, it's as simple as:

    ```
    from options import PipelineCustomOptions
    options = PipelineCustomOptions()
    with beam.Pipeline(options=options) as p:
        # whatever
    ```
    """

    @classmethod
    def _add_argparse_args(cls, parser):
        parser.add_value_provider_argument(
            "--input_file", help="Input file for the pipeline"
        )
        parser.add_value_provider_argument(
            "--output_path", help="Output path where result files will be stored"
        )
