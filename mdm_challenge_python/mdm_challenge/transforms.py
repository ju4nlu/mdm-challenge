import apache_beam as beam

from typing import NamedTuple, Optional

"""
This structure represents a column in the input CSV file.
It contains the following attributes:
    - name: name of the column in the CSV file
    - index: placement of the column, starting at 0
    - type: data type contained in the column
"""
InputCSVColumn = NamedTuple(
    "InputCSVColumn", [("name", str), ("index", int), ("type", object)]
)

# Dictionary with the relevant columns that will be used to extract the indicators.
# Key = column nickname; Value = InputCSVColumn object
RELEVANT_COLS = {
    "country": InputCSVColumn(name="cntry", index=5, type=str),
    "internet_usage": InputCSVColumn(name="netustm", index=8, type=int),
    "age": InputCSVColumn(name="agea", index=252, type=int),
    "paid_job": InputCSVColumn(name="pdjobev", index=342, type=int),
    "net_income": InputCSVColumn(name="netilet", index=509, type=int),
    "education_level": InputCSVColumn(name="eisced", index=292, type=int),
}


class KeepRelevantColumnsFn(beam.DoFn):
    """This PTransform receives as input an element of a PCollection, which is
    a text line coming from a CSV file. The file is delimited by `,`. This
    transformation returns a dictionary which only contains the relevant
    columns specified by the user.
    """

    def __init__(self, columns: list):
        """Constructor of the KeepRelevantColumns DoFn class.

        Args:
            columns (list): List of strings with the columns that will be kept.
        """
        self.columns = columns

    def process(self, element: str):
        """For a given element of the PCollection, extracts just the columns
        specified in the constructor. To extract the columns it makes use
        of the dictionary RELEVANT_ROWS, which acts as a configuration object.

        Args:
            element (str): A PCollection entry.

        Returns:
            Another PCollection entry with transformed data.
        """
        splitted_row = element.split(",")

        my_data = {}

        # We'll just focus on the desired columns
        for col in self.columns:
            # Obtain the value of the column based on its index
            column_value = splitted_row[RELEVANT_COLS[col].index]

            # Cast the value to its type and assign it to the output dictionary
            my_data[col] = RELEVANT_COLS[col].type(column_value)

        return [my_data]


class ObtainKeyPercentagesFn(beam.CombineFn):
    """This PTransform processes each of the elements of a PCollection,
    calculating the percentage of elements which belong to each key.

    For example, for an input PCollection like:
        - [MB, MB, DK]

    The output will be:
        - {MB: 0.66, DK: 0.33}

    The implementation of this PTransform has been inherited from:
    https://beam.apache.org/documentation/transforms/python/aggregation/combineglobally/#example-7-combining-with-a-combinefn
    """

    def create_accumulator(self):
        """This method is called just one time when the PTransform is called.
        It creates the empty accumulator where elements will be stored.

        The accumulator will be a dictionary where the keys are values which are
        contained in the PCollection and the values are the number of ocurrences
        of each key.

        Accumulator example:
            { key_1: n_ocurrences_1,
              key_2: n_ocurrences_2,
              ...
              key_N: n_ocurrences_Z,
            }
        """
        return {}

    def add_input(self, accumulator, input):
        """This method is called once per element. It adds the current input
        to the previously defined accumulator.

        If the element was already in the accumulator, increase its counter
        by 1. If it wasn't insert it in the accumulator and set its counter
        to 1.

        Args:
            accumulator ([type]): Accumulator with all the inputs
            input_element ([type]): Input element of the PCollection
        """
        if input not in accumulator:
            accumulator[input] = 0
        accumulator[input] += 1

        return accumulator

    def merge_accumulators(self, accumulators):
        """This method can be called zero or more times. It's called when the
        execution is scheduled in parallel, and its function is to merge all
        the independent accumulators that could be created by Beam.

        For each key in each accumulator, the merging will be done by adding
        the values of each key.

        Args:
            accumulators ([type]): List of processed accumulators
        """
        merged_accumulator = {}
        for acc in accumulators:
            for key, value in acc.items():
                if key not in merged_accumulator:
                    merged_accumulator[key] = 0
                merged_accumulator[key] += value

        return merged_accumulator

    def extract_output(self, accumulator):
        """This method is called just one time before returning the results.
        It takes all the values stored in the accumulator and calculates the
        percentage of appareances of each key. Then, returns just the results
        when the key == 1, this means that only the percentage of people
        that's ever had a paid job will be returned.

        An example of output would be:
            0.66

        Args:
            accumulator ([type]): Accumulator of the PTransform
        """
        final_averages = {}
        total_values = sum(accumulator.values())
        return {key: (value / total_values) for key, value in accumulator.items()}


class GetMostRepresentedEducationLevelPerNetIncomeFn(beam.CombineFn):
    """This PTransform has as input a PCollection for which the key of each
    element is a tuple where the first element is the net income level and
    the second one is the education level. Then, the value of each element
    is the number of input rows which had that net income and that education
    level.

    For each net income level, it extracts the most represented education
    level.

    For example, for an input PCollection like:
        - ((net_1, edu_1), 20), ((net_1, edu_2), 200), ((net_1, edu_3), 50)

    The output will be:
        - {net_1: edu_2}

    The implementation of this PTransform has been inherited from:
    https://beam.apache.org/documentation/transforms/python/aggregation/combineglobally/#example-7-combining-with-a-combinefn
    """

    def create_accumulator(self):
        """This method is called just one time when the PTransform is called.
        It creates an empty accumulator where elements will be stored.

        The accumulator will be a dictionary where the keys are the tuples formed
        by the net income level and the education level, and the values will be
        the number of observations for each combination.

        Accumulator example:
            { (net_1, edu_1): n_observations_1,
              (net_1, edu_2): n_observations_2
              ...
              (net_N, edu_M): n_observations_Z,
            }
        """
        return {}

    def add_input(self, accumulator, input):
        """This method is called once per element. It stores the number of observations
        per net income and education level observed. As we are sure there will be no
        duplicates, there's no need to check if the element was already inserted.

        The accumulator

        Args:
            accumulator ([type]): Accumulator with all the inputs
            input_element ([type]): Input element of the PCollection. It's structure
            is -> ((net_income, education_level), count)
        """
        accumulator[input[0]] = input[1]
        return accumulator

    def merge_accumulators(self, accumulators):
        """This method can be called zero or more times. It's called when the
        execution is scheduled in parallel, and its function is to merge all
        the independent accumulators that could be created by Beam.

        In this case, merging accumulators means merging the dictionaries
        into one.

        Args:
            accumulators ([type]): List of processed accumulators
        """
        merged_accumulator = {}
        for acc in accumulators:
            merged_accumulator = {**merged_accumulator, **acc}

        return merged_accumulator

    def extract_output(self, accumulator):
        """This method is called just one time before returning the results.
        It takes all the values stored and, for each net_income, it computes
        the education level that has a biggest count of rows.

        This method will return a dictionary where the keys are the different
        net income levels and the values are the most represented education
        level for each of them.

        An example of output would be:
            { net_1: edu_1,
              net_2: edu_3,
              ...
              net_N: edu_M
            }

        Args:
            accumulator ([type]): Accumulator of the PTransform
        """
        final_accumulator = {}

        for (net_income, education), count in accumulator.items():
            if net_income not in final_accumulator:
                final_accumulator[net_income] = (education, count)
            elif (
                net_income in final_accumulator
                and count > final_accumulator[net_income][1]
            ):
                final_accumulator[net_income] = (education, count)
            # Other wise the net_income is already in the accumulator with a lower count

        return {
            net_income: education
            for net_income, (education, _) in final_accumulator.items()
        }
