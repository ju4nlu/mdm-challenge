import apache_beam as beam

from mdm_challenge.options import PipelineCustomOptions
from mdm_challenge.transforms import KeepRelevantColumnsFn
from mdm_challenge.transforms import ObtainKeyPercentagesFn
from mdm_challenge.transforms import GetMostRepresentedEducationLevelPerNetIncomeFn


def run_pipelines():
    """This method executes a simple pipeline.
    This is a testing pipeline and will be changed soon in the future.

    It needs both --input and --output parameters to run, then extracts
    the average daily usage of Internet per country from the input file,
    storing the results in the desired output.
    """

    pipeline_options = PipelineCustomOptions()

    with beam.Pipeline(options=pipeline_options) as p:
        rows = p | "Read file from storage" >> beam.io.ReadFromText(
            pipeline_options.input_file, skip_header_lines=1
        )

        # On average, and per country, how much time (in minutes) does the people
        # spend using the internet on a daily basis?
        first_indicator = (
            rows
            | "Obtain country and internet usage columns"
            >> beam.ParDo(KeepRelevantColumnsFn(columns=["country", "internet_usage"]))
            | "Remove invalid values of internet usage"
            >> beam.Filter(
                lambda row: row["internet_usage"] not in [6666, 7777, 8888, 9999]
            )
            | "Transform country and internet usage to Key-Value pairs"
            >> beam.ParDo(lambda row: [(row["country"], row["internet_usage"])])
            | "Get mean internet usage by country" >> beam.combiners.Mean.PerKey()
        )

        # Overall, what is the percentage of people under 35 that ever had a paid job?
        second_indicator = (
            rows
            | "Obtain age and ever had a job columns"
            >> beam.ParDo(KeepRelevantColumnsFn(columns=["age", "paid_job"]))
            | "Remove entries with invalid age"
            >> beam.Filter(lambda row: row["age"] != 999)
            | "Keep people that's under 35" >> beam.Filter(lambda row: row["age"] < 35)
            | "Remove invalid 'ever had a job' responses"
            >> beam.Filter(lambda row: row["paid_job"] in [0, 1])
            | "Transform to boolean single values"
            >> beam.ParDo(lambda row: [row["paid_job"]])
            | "Count number of responses per age"
            >> beam.CombineGlobally(ObtainKeyPercentagesFn())
            | "Keep percentage of people that's ever had a paid job"
            >> beam.Map(lambda row: row[1])
        )

        # Considering people's net income is distributed amongst 10 categories(from 1 to 10,
        # being 1 the lowest income and 10 the highest), extract the education level most
        # represented on each category.
        third_indicator = (
            rows
            | "Obtain people's net income and education level columns"
            >> beam.ParDo(
                KeepRelevantColumnsFn(columns=["net_income", "education_level"])
            )
            | "Remove entries with invalid net income"
            >> beam.Filter(lambda row: row["net_income"] not in [66, 77, 88, 99])
            | "Remove entries with invalid education level"
            >> beam.Filter(lambda row: row["education_level"] not in [77, 88, 99])
            | "Group net income and education levels"
            >> beam.ParDo(
                lambda row: [((row["net_income"], row["education_level"]), 1)]
            )
            | "Count per key" >> beam.combiners.Count.PerKey()
            | "Most represented education level per net income"
            >> beam.CombineGlobally(GetMostRepresentedEducationLevelPerNetIncomeFn())
        )

        # Write the results of each pipeline to the specified directory
        first_indicator | "Write first indicator results to external storage" >> beam.io.WriteToText(
            file_path_prefix=f"{pipeline_options.output_path}/1_indicator.txt",
            shard_name_template="",
        )

        second_indicator | "Write second indicator results to external storage" >> beam.io.WriteToText(
            file_path_prefix=f"{pipeline_options.output_path}/2_indicator.txt",
            shard_name_template="",
        )

        third_indicator | "Write third indicator results to external storage" >> beam.io.WriteToText(
            file_path_prefix=f"{pipeline_options.output_path}/3_indicator.txt",
            shard_name_template="",
        )


if __name__ == "__main__":
    run_pipelines()
