#!/usr/bin/env python

import setuptools

setuptools.setup(
    name="mdm_challenge",
    version="1.0",
    description="MDM Data Challenge code",
    author="Juan Luis",
    author_email="juanluisgomezchanclon@gmail.com",
    url="https://gitlab.com/ju4nlu/mdm-challenge",
    packages=setuptools.find_packages(),
    python_requires="==3.7.9",
    install_requires="apache-beam[gcp,test]==2.27.0",
    test_suite="tests",
)
