import unittest
import apache_beam as beam

from apache_beam.testing import test_pipeline
from apache_beam.testing.util import assert_that, equal_to

from mdm_challenge.transforms import KeepRelevantColumnsFn
from mdm_challenge.transforms import ObtainKeyPercentagesFn
from mdm_challenge.transforms import GetMostRepresentedEducationLevelPerNetIncomeFn


class TestPTransforms(unittest.TestCase):
    def test_column_extractor_ptransform(self):
        """Checks that the PTransformation KeepRelevantColumnsFn correctly obtains
        the specified columns and their values.
        """
        # Sample input rows, the columns names are:
        # name,essround,edition,proddate,idno,cntry,nwspol,netusoft,netustm,ppltrst
        input_rows = [
            "ESS9e03,9,3,10.12.2020,27,SAMPLE,60,5,180,2",
            "ESS9e03,9,3,10.12.2020,137,SAMPLE,10,5,20,7",
            "ESS9e03,9,3,10.12.2020,194,SAMPLE,60,4,0,5",
        ]

        expected_output = [
            {"country": "SAMPLE", "internet_usage": 180},
            {"country": "SAMPLE", "internet_usage": 20},
            {"country": "SAMPLE", "internet_usage": 0},
        ]

        with test_pipeline.TestPipeline() as p:

            # Create the input collection from an in-memory object
            input_collection = p | beam.Create(input_rows)

            # Apply the PTransform
            transformed_collection = input_collection | beam.ParDo(
                KeepRelevantColumnsFn(columns=["country", "internet_usage"])
            )

            # Compare the output to the expected output
            assert_that(transformed_collection, equal_to(expected_output))

    def test_percentages_ptransform_two_keys(self):
        """Checks that the PTransformation ObtainKeyPercentagesFn correctly obtains
        the percentages for each key when there are more than 1 key present.
        """
        input_elements = ["tres", "tres", "tres", "dos", "dos"]

        expected_output = [{"tres": 0.6, "dos": 0.4}]

        with test_pipeline.TestPipeline() as p:
            # Create the input collection from an in-memory object
            input_collection = p | beam.Create(input_elements)

            # Apply the PTransform
            transformed_collection = input_collection | beam.CombineGlobally(
                ObtainKeyPercentagesFn()
            )

            # Compare the output to the expected output
            assert_that(transformed_collection, equal_to(expected_output))

    def test_percentages_ptransform_no_keys(self):
        """Checks that the PTransformation ObtainKeyPercentagesFn doesn't raise
        an Exception when there are no elements in the input PCollection and
        correctly returns an empty value.
        """
        input_elements = []

        expected_output = [{}]

        with test_pipeline.TestPipeline() as p:
            # Create the input collection from an in-memory object
            input_collection = p | beam.Create(input_elements)

            # Apply the PTransform
            transformed_collection = input_collection | beam.CombineGlobally(
                ObtainKeyPercentagesFn()
            )

            # Compare the output to the expected output
            assert_that(transformed_collection, equal_to(expected_output))

    def test_net_income_education_level_ptransform(self):
        """Checks that the PTransformation GetMostRepresentedEducationLevelPerNetIncomeFn
        correctly obtains the most represented education level per net income level.
        """
        input_elements = [
            (("net_1", "edu_1"), 20),
            (("net_1", "edu_2"), 200),
            (("net_1", "edu_3"), 4),
            (("net_1", "edu_4"), 89),
            (("net_2", "edu_1"), 20),
        ]

        expected_output = [{"net_1": "edu_2", "net_2": "edu_1"}]

        with test_pipeline.TestPipeline() as p:
            # Create the input collection from an in-memory object
            input_collection = p | beam.Create(input_elements)

            # Apply the PTransform
            transformed_collection = input_collection | beam.CombineGlobally(
                GetMostRepresentedEducationLevelPerNetIncomeFn()
            )

            # Compare the output to the expected output
            assert_that(transformed_collection, equal_to(expected_output))

    def test_net_income_education_level_ptransform_no_keys(self):
        """Checks that the PTransformation GetMostRepresentedEducationLevelPerNetIncomeFn
        doesn't raise an Exception when there are no elements in the input PCollection and
        correctly returns an empty value.
        """
        input_elements = []

        expected_output = [{}]

        with test_pipeline.TestPipeline() as p:
            # Create the input collection from an in-memory object
            input_collection = p | beam.Create(input_elements)

            # Apply the PTransform
            transformed_collection = input_collection | beam.CombineGlobally(
                GetMostRepresentedEducationLevelPerNetIncomeFn()
            )

            # Compare the output to the expected output
            assert_that(transformed_collection, equal_to(expected_output))
